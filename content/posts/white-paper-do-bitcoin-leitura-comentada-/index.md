---
title: "White Paper do Bitcoin [Leitura Comentada]"
date: 2023-06-20T18:04:23-03:00
summary: " Neste vídeo, faço a leitura comentada do White Paper do Bitcoin, passando ponto a ponto sobre seu conceito, sobre como funciona e quais problemas ele resolve."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/white-paper-bitcoin:d" >}} 

Lbry: lbry://@CypherpunksToday#3/white-paper-bitcoin#d

Odysee: https://odysee.com/@CypherpunksToday:3/white-paper-bitcoin:d


White Paper: https://bitcoin.org/files/bitcoin-paper/bitcoin_pt_br.pdf

## Description

Neste vídeo, faço a leitura comentada do White Paper do Bitcoin, passando ponto a ponto sobre seu conceito, sobre como funciona e quais problemas ele resolve.

# Attributions and Special Thanks

## Opening track: Impact Event (by LUKHASH)

More about LUKHASH: 

 - Website: https://www.lukhash.com/#!licensing.html
 - Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
 - YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: Rasengan (by Turbo Knight)

More about Turbo Knight:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao Cypherpunks Brasil (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to MrSuicideSheep for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about MrSuicideSheep:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


