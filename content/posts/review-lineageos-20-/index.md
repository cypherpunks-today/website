---
title: "Review LineageOS 20"
date: 2023-06-27T20:34:56-03:00
summary: " Neste vídeo, trago um review do LineageOS 20 rodando no meu Motorola Edge 30, mostrando suas funcionalidades, diferenciais e vantagens."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/review-lineage-os-20:b" >}} 

Lbry: lbry://@CypherpunksToday#3/review-lineage-os-20#b

Odysee: https://odysee.com/@CypherpunksToday:3/review-lineage-os-20:b


LineageOS: https://lineageos.org/

## Description

Neste vídeo, trago um review do LineageOS 20 rodando no meu Motorola Edge 30, mostrando suas funcionalidades, diferenciais e vantagens.

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


