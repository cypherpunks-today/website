---
title: "\"Considerações Finais sobre como Sobreviver à Distopia Digital\" [ Leitura Comentada ]"
date: 2023-07-18T19:40:47-03:00
summary: " O destino da humanidade é dado pela somatória de todas as ações humanas.  A construção de um mundo cripto-anarquista depende da ação individual de cada um, depende do exemplo que cada um dá aos seus pares e da capacidade de se construir núcleos de sociedade que sejam livres e independentes do Estado.  Neste vídeo, trago uma leitura comentada do artigo [Considerações Finais sobre como Sobreviver à Distopia Digital - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/consideracoes-finais-sobre-como-sobreviver-a-distopia-digital/)"
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/como-sobreviver-a-distopia-digital:f" >}} 

Lbry: lbry://@CypherpunksToday#3/como-sobreviver-a-distopia-digital#f

Odysee: https://odysee.com/@CypherpunksToday:3/como-sobreviver-a-distopia-digital:f


Artigo Traduzido: https://cypherpunks.com.br/documentos/consideracoes-finais-sobre-como-sobreviver-a-distopia-digital/

## Description

O destino da humanidade é dado pela somatória de todas as ações humanas.

A construção de um mundo cripto-anarquista depende da ação individual de cada um, depende do exemplo que cada um dá aos seus pares e da capacidade de se construir núcleos de sociedade que sejam livres e independentes do Estado.

Neste vídeo, trago uma leitura comentada do artigo [Considerações Finais sobre como Sobreviver à Distopia Digital - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/consideracoes-finais-sobre-como-sobreviver-a-distopia-digital/)

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


