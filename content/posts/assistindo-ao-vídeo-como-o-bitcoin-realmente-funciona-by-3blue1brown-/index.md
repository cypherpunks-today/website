---
title: "Assistindo ao vídeo \"Como o Bitcoin realmente funciona?\" (by 3Blue1Brown)"
date: 2023-06-21T19:32:09-03:00
summary: " Neste vídeo, assisto ao vídeo \"But how dows bitcoin actually work?\" do canal 3Blue1Brown, para melhor entender o funcionamento do Bitcoin e complementar nosso último vídeo, no qual fizemos uma leitura comentada do White Paper do Bitcoin."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/assistindo-como-bitcoin-realmente-funciona-3blue1brown:d" >}} 

Lbry: lbry://@CypherpunksToday#3/assistindo-como-bitcoin-realmente-funciona-3blue1brown#d

Odysee: https://odysee.com/@CypherpunksToday:3/assistindo-como-bitcoin-realmente-funciona-3blue1brown:d


Vídeo original by 3Blue1Brown: [But how does bitcoin actually work? - YouTube](https://youtu.be/bBC-nXj3Ng4)

## Description

Neste vídeo, assisto ao vídeo "But how dows bitcoin actually work?" do canal 3Blue1Brown, para melhor entender o funcionamento do Bitcoin e complementar nosso último vídeo, no qual fizemos uma leitura comentada do White Paper do Bitcoin.

# Attributions and Special Thanks

## Opening track: Impact Event (by LUKHASH)

More about LUKHASH:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: Rasengan (by Turbo Knight)

More about Turbo Knight:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about MrSuicideSheep:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg

Special thanks to **3Blue1Brown** for the amazing video on how Bitcoin works.

- Original video: https://www.youtube.com/watch?v=bBC-nXj3Ng4


