---
title: "Ross Ulbricht e a Coragem de Desrespeitar a Lei [ Leitura Comentada ]"
date: 2023-07-14T16:16:40-03:00
summary: " Neste vídeo, trago uma Leitura Comentada de um artigo chamado \"Ross Ulbricht e a Coragem de Desrespeitar a Lei\" do site Cypherpunks Brasil.  Falamos sobre a coragem e o dever moral de se desrespeitar leis artificiais injustas, de modo a trazer luz sobre as leis naturais justas.  O artigo mostra que Ross Ulbricht não foi condenado por ter criado um site através do qual terceiros vendiam muitas coisas, inclusive drogas.  Ulbricht foi condenado por ter passado por cima das leis injustas e sem sentido, dando origem a um verdadeiro livre mercado, no qual as pessoas podiam negociar livremente utilizando um sistema de reputação similar aos dos sites Mercado Livre e Ebay.  É interessante notar que, todos aqueles que foram punidos pelo Estado por desrespeitarem leis artificiais que em dado momento estavam vigentes, hoje são aclamados e chamados de heróis por políticos que condenam, no presente momento, pessoas que estão dando suas vidas pra acabarem com leis artificiais injustas que estão vigentes no presente momento.  Hoje são chamados de criminosos, amanhã serão chamados de heróis. Pois pra maioria desinformada, a linha que demarca a fronteira entre o certo e o errado não está na lógica, na razão ou no bom senso, mas sim está num papelzinho estatal rabiscado por políticos."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/ross-ulbricht-coragem-desrespeitar-leis:b" >}} 

Lbry: lbry://@CypherpunksToday#3/ross-ulbricht-coragem-desrespeitar-leis#b

Odysee: https://odysee.com/@CypherpunksToday:3/ross-ulbricht-coragem-desrespeitar-leis:b


Artigo traduzido: https://cypherpunks.com.br/documentos/ross-ulbricht-e-a-coragem-de-desrespeitar-a-lei/

Artigo original: https://c4ss.org/content/53744

## Description

Neste vídeo, trago uma Leitura Comentada de um artigo chamado "Ross Ulbricht e a Coragem de Desrespeitar a Lei" do site Cypherpunks Brasil.

Falamos sobre a coragem e o dever moral de se desrespeitar leis artificiais injustas, de modo a trazer luz sobre as leis naturais justas.

O artigo mostra que Ross Ulbricht não foi condenado por ter criado um site através do qual terceiros vendiam muitas coisas, inclusive drogas.

Ulbricht foi condenado por ter passado por cima das leis injustas e sem sentido, dando origem a um verdadeiro livre mercado, no qual as pessoas podiam negociar livremente utilizando um sistema de reputação similar aos dos sites Mercado Livre e Ebay.

É interessante notar que, todos aqueles que foram punidos pelo Estado por desrespeitarem leis artificiais que em dado momento estavam vigentes, hoje são aclamados e chamados de heróis por políticos que condenam, no presente momento, pessoas que estão dando suas vidas pra acabarem com leis artificiais injustas que estão vigentes no presente momento.

Hoje são chamados de criminosos, amanhã serão chamados de heróis. Pois pra maioria desinformada, a linha que demarca a fronteira entre o certo e o errado não está na lógica, na razão ou no bom senso, mas sim está num papelzinho estatal rabiscado por políticos.

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


