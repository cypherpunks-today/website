---
title: "internets em letra minúscula [ Leitura Comentada ]"
date: 2023-06-23T21:24:19-03:00
summary: " Neste vídeo, faço uma leitura comentada do artigo \"Como podemos reconquistar a Internet criando internets em letra minúscula\" do site [Cypherpunks Brasil](https://cypherpunks.com.br).  O artigo se propõe a mostrar que é possível construir internets alternativas, que não são controladas pelas Big Techs e governos.  Ele mostra que também é possível conectar várias internets e criar super redes, que inclusive podem usar da Internet tradicional como um meio de comunicação entre elas, utilizando criptografia para envolver os dados trafegados, de forma a esconder esses dados das Big Techs e governos.  Também ressalta a necessidade de mais profissionais e voluntários que possam contribuir para a construção de uma infraestrutura livre, independente e mais privada."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/internets-em-letra-minuscula:d" >}} 

Lbry: lbry://@CypherpunksToday#3/internets-em-letra-minuscula#d

Odysee: https://odysee.com/@CypherpunksToday:3/internets-em-letra-minuscula:d


Artigo traduzido: [Como podemos reconquistar a Internet criando internets em letra minúscula - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/como-reconquistar-a-internet/)

Artigo original: [Center for a Stateless Society &raquo; How we can win back the Internet by creating lowercase internets](https://c4ss.org/content/53903)

## Description

Neste vídeo, faço uma leitura comentada do artigo "Como podemos reconquistar a Internet criando internets em letra minúscula" do site [Cypherpunks Brasil](https://cypherpunks.com.br).

O artigo se propõe a mostrar que é possível construir internets alternativas, que não são controladas pelas Big Techs e governos.

Ele mostra que também é possível conectar várias internets e criar super redes, que inclusive podem usar da Internet tradicional como um meio de comunicação entre elas, utilizando criptografia para envolver os dados trafegados, de forma a esconder esses dados das Big Techs e governos.

Também ressalta a necessidade de mais profissionais e voluntários que possam contribuir para a construção de uma infraestrutura livre, independente e mais privada.

# Attributions and Special Thanks

## Opening track: Impact Event (by LUKHASH)

More about LUKHASH:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: Rasengan (by Turbo Knight)

More about Turbo Knight:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about MrSuicideSheep:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


