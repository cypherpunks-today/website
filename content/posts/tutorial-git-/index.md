---
title: "Tutorial Git"
date: 2023-07-27T22:22:58-03:00
summary: " Git é um Sistema de Controle de Versões livre, gratuito e de código aberto.  É amplamente utilizado por projetos de código aberto que buscam um meio para gerenciar seus projetos de forma distribuída, ao invés dos modelos proprietários que centralizam todo o processo de desenvolvimento.  É inegociável saber Git para contribuir com projetos de código aberto, e trago a vocês um tutorial de Git, para facilitar seu ingresso no mundo Open Source."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/tutorial-git:a" >}} 

Lbry: lbry://@CypherpunksToday#3/tutorial-git#a

Odysee: https://odysee.com/@CypherpunksToday:3/tutorial-git:a


Site do Git: https://git-scm.com

## Description

Git é um Sistema de Controle de Versões livre, gratuito e de código aberto.

É amplamente utilizado por projetos de código aberto que buscam um meio para gerenciar seus projetos de forma distribuída, ao invés dos modelos proprietários que centralizam todo o processo de desenvolvimento.

É inegociável saber Git para contribuir com projetos de código aberto, e trago a vocês um tutorial de Git, para facilitar seu ingresso no mundo Open Source.

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


