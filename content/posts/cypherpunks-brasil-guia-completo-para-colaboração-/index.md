---
title: "Cypherpunks Brasil: Guia Completo para Colaboração"
date: 2023-06-22T20:00:50-03:00
summary: " Neste vídeo, ensino o passo a passo de como contribuir com a cena Cypherpunk no Brasil usando o guia de colaboração do movimento Cypherpunks Brasil ( https://cypherpunks.com.br )."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/cypherpunks-brasil-guia-colaboracao:3" >}} 

Lbry: lbry://@CypherpunksToday#3/cypherpunks-brasil-guia-colaboracao#3

Odysee: https://odysee.com/@CypherpunksToday:3/cypherpunks-brasil-guia-colaboracao:3


Guia original: [Faça Parte: Guia Para Colaboração - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/contribuir-cypherpunksbr/)

Tutorial GitHub original: [Tutorial de uso do Github via website - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/wiki-gitub-browser/)

## Description

Neste vídeo, ensino o passo a passo de como contribuir com a cena Cypherpunk no Brasil usando o guia de colaboração do movimento Cypherpunks Brasil ( https://cypherpunks.com.br ).

### Seções do vídeo:

01:27 - Como ajudar o Cypherpunks Brasil

02:10 - O básico de contribuição em código aberto usando GitHub

03:48 - Tutorial de uso do GitHub via website

04:13 - Criando um Fork

07:11 - Criando uma nova Branch

09:05 - Renomeando e movendo arquivos

19:45 - Criando um Pull Request

24:10 - Abrindo uma Issue

27:21 - Adicionando Novos Documentos

31:54 - Traduzindo Documentos para Português (pt-br)

37:10 - Revisando e corrigindo Documentos traduzidos

38:27 - Melhorando nosso site

39:14 - Crie novos projetos

39:40 - Contribuindo no mundo real

# Attributions and Special Thanks

## Opening track: Impact Event (by LUKHASH)

More about LUKHASH:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: Rasengan (by Turbo Knight)

More about Turbo Knight:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about MrSuicideSheep:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


