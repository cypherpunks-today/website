---
title: "Contra-Economia: Células de Liberdade [ Leitura Comentada ]"
date: 2023-07-11T17:05:23-03:00
summary: " Contra-Economia é uma filosofia de ação prática e funcional para a contrução de sociedades paralelas e independentes do Estado.  Um dos meios idealizados para se alcançar tal feito são as Células de Liberdade, as quais são compostas de 7 a 9 pessoas, com foco na construção de comportamentos e ambientes auto sustentáveis e que permitam a auto defesa.  A ideia consiste de ter grupos de pessoas que exerçam atividades que elevem todos os seus membros a um patamar de independência, sobre o qual pode-se construir um ambiente propício para a vida e para a livre atividade econômica.  Após a consolidação de uma célula, seus membros são orientados e incentivados a construirem outras células, de forma a dar origem a uma rede de células de liberdade, permitindo assim uma descentralização e a eliminação de um ponto central de falha.  As células têm como objetivo garantir que todos saibam o básico sobre primeiros socorros, uso defensivo de armas de fogo, treinamento para reduzir o tempo de resposta em casos de emergência, além de garantir o básico sobre produção de alimentos, dentre outros comportamentos essenciais para uma vida em sociedades de resistência.  O objetivo posto no texto se trata de criar uma rede de células de liberdade que auxiliem umas às outras, façam comércio entre si e consigam criar uma sociedade fora da malha tecnocrática, na qual o Estado e suas empresas aliadas vigiam cada passo que você dá, com o intuito de te tornar um gado obediente do sistema cujo único objetivo de vida é alimentar esse sistema."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/celulas-de-liberdade:6" >}} 

Lbry: lbry://@CypherpunksToday#3/celulas-de-liberdade#6

Odysee: https://odysee.com/@CypherpunksToday:3/celulas-de-liberdade:6


Artigo traduzido: [A comunidade contra-econômica: células de liberdade - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/a-comunidade-contra-econ%C3%B4mica-c%C3%A9lulas-de-liberdade/)

## Description

Contra-Economia é uma filosofia de ação prática e funcional para a contrução de sociedades paralelas e independentes do Estado.

Um dos meios idealizados para se alcançar tal feito são as Células de Liberdade, as quais são compostas de 7 a 9 pessoas, com foco na construção de comportamentos e ambientes auto sustentáveis e que permitam a auto defesa.

A ideia consiste de ter grupos de pessoas que exerçam atividades que elevem todos os seus membros a um patamar de independência, sobre o qual pode-se construir um ambiente propício para a vida e para a livre atividade econômica.

Após a consolidação de uma célula, seus membros são orientados e incentivados a construirem outras células, de forma a dar origem a uma rede de células de liberdade, permitindo assim uma descentralização e a eliminação de um ponto central de falha.

As células têm como objetivo garantir que todos saibam o básico sobre primeiros socorros, uso defensivo de armas de fogo, treinamento para reduzir o tempo de resposta em casos de emergência, além de garantir o básico sobre produção de alimentos, dentre outros comportamentos essenciais para uma vida em sociedades de resistência.

O objetivo posto no texto se trata de criar uma rede de células de liberdade que auxiliem umas às outras, façam comércio entre si e consigam criar uma sociedade fora da malha tecnocrática, na qual o Estado e suas empresas aliadas vigiam cada passo que você dá, com o intuito de te tornar um gado obediente do sistema cujo único objetivo de vida é alimentar esse sistema.

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


