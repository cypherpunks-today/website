---
title: "Introdução ao Agorismo & Obstáculos e Soluções na Contra-Economia [ Leitura Comentada ]"
date: 2023-08-23T20:42:15-03:00
summary: " Agorismo e Contra-Economia são conceitos fundamentais na vida de qualquer libertário que não quer esperar pelo fim do Estado para ser livre.  Neste vídeo, trago uma leitura comentada de 2 artigos do site Cypherpunks Brasil.  Trago também algumas discordâncias com os textos, mas sempre no intuito de reduzir os ruídos nas comunicações, para que os sinais possam se ampliar."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/intro-agorismo-obstaculos-solucoes-vida-contra-economica:4" >}} 

Lbry: lbry://@CypherpunksToday#3/intro-agorismo-obstaculos-solucoes-vida-contra-economica#4

Odysee: https://odysee.com/@CypherpunksToday:3/intro-agorismo-obstaculos-solucoes-vida-contra-economica:4


Primeiro artigo: [Uma pequena introdução ao agorismo e ao Samuel Edward Konkin III - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/uma-pequena-introducao-ao-agorismo-e-sek3/)

Segundo artigo: [Os Obstáculos(e soluções) para Viver o Estilo de Vida Contra-Econômico - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/os-obst%C3%A1culos-e-solu%C3%A7%C3%B5es-para-viver-o-estilo-de-vida-contra-econ%C3%B4mico/)

## Description

Agorismo e Contra-Economia são conceitos fundamentais na vida de qualquer libertário que não quer esperar pelo fim do Estado para ser livre.

Neste vídeo, trago uma leitura comentada de 2 artigos do site Cypherpunks Brasil.

Trago também algumas discordâncias com os textos, mas sempre no intuito de reduzir os ruídos nas comunicações, para que os sinais possam se ampliar.

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


