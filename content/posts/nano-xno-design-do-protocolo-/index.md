---
title: "Nano XNO: Design do Protocolo"
date: 2023-09-19T00:20:51-03:00
summary: " A Nano é uma criptomoeda diferente das outras: sua estrutura é a Block-Lattice.  Block-Lattice é um conjunto de Blockchains, no qual cada Blockchain é uma conta de usuário.  Neste vídeo, vemos detalhes de como a Nano funciona, incluindo diagramas que mostram como são vários de seus processos.  Na primeira seção do vídeo, recapitulamos todo o conteúdo do último vídeo, e na segunda seção, avançamos para os tópicos  - Spam, Trabalho e Priorização    - Detalhes da Rede    - Open Representative Voting   "
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/nano-white-paper-pt2:a" >}} 

Lbry: lbry://@CypherpunksToday#3/nano-white-paper-pt2#a

Odysee: https://odysee.com/@CypherpunksToday:3/nano-white-paper-pt2:a


Living Whitepaper da Nano: [Living Whitepaper - Nano Documentation](https://docs.nano.org/living-whitepaper/)

- Primeira Seção: 02:30
- Segunda Seção: 31:24

Meu endereço Nano: nano_3qsrgpa8q6rgj971f3yweewpekq4g5wncpz6hpj1hmjfwzsgzb79eweioyir

Primeiro vídeo: https://odysee.com/@CypherpunksToday:3/nano-white-paper-pt1:8

Anotações do vídeo e diagramas: https://gitlab.com/darwin.brandao/nano-design-do-protocolo

## Description

A Nano é uma criptomoeda diferente das outras: sua estrutura é a Block-Lattice.

Block-Lattice é um conjunto de Blockchains, no qual cada Blockchain é uma conta de usuário.

Neste vídeo, vemos detalhes de como a Nano funciona, incluindo diagramas que mostram como são vários de seus processos.

Na primeira seção do vídeo, recapitulamos todo o conteúdo do último vídeo, e na segunda seção, avançamos para os tópicos

- Spam, Trabalho e Priorização
  
- Detalhes da Rede
  
- Open Representative Voting
  

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


