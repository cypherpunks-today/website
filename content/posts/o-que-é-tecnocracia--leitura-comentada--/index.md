---
title: "\"O que é Tecnocracia\" [ Leitura Comentada ]"
date: 2023-08-01T21:54:10-03:00
summary: " Tecnocracia é o pesadelo dos Cypherpunks.  Um mundo dominado pela elite através do uso da tecnologia e de técnicos, escravizando toda a humanidade num sistema de engenharia social, tratando a sociedade como uma máquia que serve unicamente para serví-los.  Parece ficção, mas todos os dias vemos novos passos nessa direção. Passos mais certeiros e eficientes, que estão muito acima da capacidade do gado médio perceber.  Neste vídeo, falamos sobre a história da Tecnocracia, o que ela é e quais são seus sinais no mundo de hoje."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/oq-e-tecnocracia:f" >}} 

Lbry: lbry://@CypherpunksToday#3/oq-e-tecnocracia#f

Odysee: https://odysee.com/@CypherpunksToday:3/oq-e-tecnocracia:f


Artigo Traduzido: [O Que é Tecnocracia - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/o-que-e-tecnocracia/)

## Description

Tecnocracia é o pesadelo dos Cypherpunks.

Um mundo dominado pela elite através do uso da tecnologia e de técnicos, escravizando toda a humanidade num sistema de engenharia social, tratando a sociedade como uma máquia que serve unicamente para serví-los.

Parece ficção, mas todos os dias vemos novos passos nessa direção. Passos mais certeiros e eficientes, que estão muito acima da capacidade do gado médio perceber.

Neste vídeo, falamos sobre a história da Tecnocracia, o que ela é e quais são seus sinais no mundo de hoje.

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfgt


