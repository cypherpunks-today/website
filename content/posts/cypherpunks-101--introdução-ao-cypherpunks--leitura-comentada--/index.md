---
title: "\"Cypherpunks 101 - Introdução ao Cypherpunks\" [ Leitura Comentada ]"
date: 2023-08-08T20:11:59-03:00
summary: " Há tempos que nos preocupamos com a escalada de poder e controle exercidos pelo Estado e Grandes Corporações.  Muitas tecnologias foram criadas e disseminadas para que pudessemos nos proteger disso.  Mas o que poucos sabem é: uma parcela significativa dessas tecnologias foram criadas pelos Cypherpunks, com o objetivo de criar estruturas digitais capazes de dar origem a novas estruturas sociais, que respeitem a Privacidade e a Liberdade por padrão."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/cypherpunks-101:a" >}} 

Lbry: lbry://@CypherpunksToday#3/cypherpunks-101#a

Odysee: https://odysee.com/@CypherpunksToday:3/cypherpunks-101:a


Artigo traduzido: https://cypherpunks.com.br/documentos/cypherpunks-101/

Artigo original:

- Parte 1: https://cypherpunkguild.medium.com/cypherpunks-101-ep-1-82d91f13aa2
  
- Parte 2: https://cypherpunkguild.medium.com/cypherpunks-101-ep-2-314bf6f054a2
  
- Parte 3: https://cypherpunkguild.medium.com/cypherpunks-101-ep-3-common-myths-and-misconceptions-about-cypherpunks-260b2f5f72eb
  

## Description

Há tempos que nos preocupamos com a escalada de poder e controle exercidos pelo Estado e Grandes Corporações.

Muitas tecnologias foram criadas e disseminadas para que pudessemos nos proteger disso.

Mas o que poucos sabem é: uma parcela significativa dessas tecnologias foram criadas pelos Cypherpunks, com o objetivo de criar estruturas digitais capazes de dar origem a novas estruturas sociais, que respeitem a Privacidade e a Liberdade por padrão.

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


