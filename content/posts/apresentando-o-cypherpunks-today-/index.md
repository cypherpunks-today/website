---
title: "Apresentando o Cypherpunks Today"
date: 2023-06-15T18:36:29-03:00
summary: "Não há descrição para este vídeo"
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/apresentando-o-cypherpunks-today:a" >}} 

Lbry: lbry://@CypherpunksToday#3/apresentando-o-cypherpunks-today#a

Odysee: https://odysee.com/@CypherpunksToday:3/apresentando-o-cypherpunks-today:a


Neste vídeo, falo sobre do que se trata o canal e o que se deve esperar.

Este canal tem como objetivo disseminar ideias Cypherpunks e Cripto-Anarquistas dando muita ênfase na ação.

Ideias até podem iluminar a escuridão, mas nada mudará até que se dê o primeiro passo.

Este é um canal sobre

- Liberdade
- Segurança
- Privacidade

Tenho como objetivo ensinar a usar ferramentas para se proteger da vigilância em massa, ensinar como contribuir com o desenvolvimento de Software Livre, Gratuito e de Código Aberto (FLOSS: Free, Libre, and Open Source Software) e contar um pouco sobre o impacto que os cypherpunks e cripto-anarquistas tiveram e têm na história.

Vale lembrar que não sou porta-voz de nenhum movimento, sou apenas um indivíduo dando sua visão de mundo acerca dos temas abordados. Minhas opiniões não necessariamente refletem as opiniões de outros cypherpunks e cripto-anarquistas. Não sou um coletivo condensado em um indivíduo, mas sim um indivíduo com ideias próprias associado a um coletivo.

Então, não esperem que minhas opiniões sejam 100% compatíveis com nenhum destes movimentos. E sintam-se livres para apontar erros contradições e demais informações que possam enriquecer o debate, mas sempre de forma educada (ninguém tem a obrigação de concordar com você).

# Attributions and Special Thanks

## Opening track: Impact Event (by LUKHASH)

More about LUKHASH: 

 - Website: https://www.lukhash.com/#!licensing.html
 - Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
 - YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: Rasengan (by Turbo Knight)

More about Turbo Knight:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao Cypherpunks Brasil (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to MrSuicideSheep for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about MrSuicideSheep:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


