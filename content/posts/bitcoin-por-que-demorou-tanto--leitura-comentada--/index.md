---
title: "Bitcoin, por que demorou tanto? [ Leitura Comentada ]"
date: 2023-06-28T19:49:05-03:00
summary: " Neste vídeo, faço uma Leitura Comentada do artigo \"Bitcoin, por que você demorou tanto?\", traduzido e publicado por Cypherpunks Brasil.  Nele, o autor discorre sobre a visão que se tinha da impossibilidade de criar uma moeda digital que pudesse competir com as moedas fiduciárias.  Ele passa por fatores como a influência da academia na visão de especialistas em criptografia, pela descrença de até mesmo criptólogos libertários, além da questão da demanda limitada que havia por uma moeda digital."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/bitcoin-porque-demorou-tanto:8" >}} 

Lbry: lbry://@CypherpunksToday#3/bitcoin-porque-demorou-tanto#8

Odysee: https://odysee.com/@CypherpunksToday:3/bitcoin-porque-demorou-tanto:8


Artigo traduzido: [Bitcoin, por que você demorou tanto? - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/bitcoin-porque-demoraste-tanto/)

Artigo original: [Unenumerated: Bitcoin, what took ye so long?](https://unenumerated.blogspot.com/2011/05/bitcoin-what-took-ye-so-long.html)

## Description

Neste vídeo, faço uma Leitura Comentada do artigo "Bitcoin, por que você demorou tanto?", traduzido e publicado por Cypherpunks Brasil.

Nele, o autor discorre sobre a visão que se tinha da impossibilidade de criar uma moeda digital que pudesse competir com as moedas fiduciárias.

Ele passa por fatores como a influência da academia na visão de especialistas em criptografia, pela descrença de até mesmo criptólogos libertários, além da questão da demanda limitada que havia por uma moeda digital.

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


