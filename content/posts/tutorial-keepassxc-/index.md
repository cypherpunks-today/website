---
title: "Tutorial KeePassXC"
date: 2023-07-12T18:09:01-03:00
summary: " Neste vídeo, eu trago um tutorial de como utilizar o gerenciador de senhas KeepassXC, um software livre, gratuito e de código aberto que te permite gerenciar suas senhas de forma offline."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/tutorial-keepassxc:5" >}} 

Lbry: lbry://@CypherpunksToday#3/tutorial-keepassxc#5

Odysee: https://odysee.com/@CypherpunksToday:3/tutorial-keepassxc:5


Link KeePassXC: https://keepassxc.org/

GitHub KeePassXC: https://github.com/keepassxreboot/keepassxc

## Description

Neste vídeo, eu trago um tutorial de como utilizar o gerenciador de senhas KeepassXC, um software livre, gratuito e de código aberto que te permite gerenciar suas senhas de forma offline.

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


