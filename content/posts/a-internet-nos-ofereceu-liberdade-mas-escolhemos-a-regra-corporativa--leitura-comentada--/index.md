---
title: "A Internet nos ofereceu Liberdade, mas escolhemos a Regra Corporativa [ Leitura Comentada ]"
date: 2023-06-26T18:53:10-03:00
summary: " Neste vídeo, faço uma leitura comentada do artigo \"A Internet nos Ofereceu Liberdade, Mas Nós Escolhemos a Regra Corporativa\" do site [Cypherpunks Brasil](https://cypherpunks.com.br).  O artigo mostra que, mesmo embora a Internet tenha nos proporcionado a possibilidade de contruírmos um mundo mais livre, acabamos optando pelo caminho mais fácil: aceitar a conveniência das regras corporativas em troca de nossas liberdades.  Isso centralizou o pode nas mãos das Big Techs, que são o braço do governo responsável pela vigilância em massa.  Deixamos de lado progamas livres e de código aberto para pagarmos por programas efêmeros e de código fechado, que ocultam o real interesse das Big Techs em seus dispositivos.  Mas mesmo embora tenhamos recusado a oferta, ela ainda está lá, e está cada dia mais atraente, na medida que descobrimos um pouco mais das reais intenções das Big Techs com nossos dispositivos a cada dia que passa."
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/internet-liberdade-regras-corporativas:f" >}} 

Lbry: lbry://@CypherpunksToday#3/internet-liberdade-regras-corporativas#f

Odysee: https://odysee.com/@CypherpunksToday:3/internet-liberdade-regras-corporativas:f


Artigo traduzido: [A Internet nos Ofereceu Liberdade, Mas Nós Escolhemos a Regra Corporativa - Cypherpunks Brasil](https://cypherpunks.com.br/documentos/a-internet-ofereceu-liberdade-escolhemos-regras-corporativas/)

Artigo original: [Center for a Stateless Society &raquo; The Internet Offered Us Freedom, We Chose Corporate Rule](https://c4ss.org/content/53608)

## Description

Neste vídeo, faço uma leitura comentada do artigo "A Internet nos Ofereceu Liberdade, Mas Nós Escolhemos a Regra Corporativa" do site [Cypherpunks Brasil](https://cypherpunks.com.br).

O artigo mostra que, mesmo embora a Internet tenha nos proporcionado a possibilidade de contruírmos um mundo mais livre, acabamos optando pelo caminho mais fácil: aceitar a conveniência das regras corporativas em troca de nossas liberdades.

Isso centralizou o pode nas mãos das Big Techs, que são o braço do governo responsável pela vigilância em massa.

Deixamos de lado progamas livres e de código aberto para pagarmos por programas efêmeros e de código fechado, que ocultam o real interesse das Big Techs em seus dispositivos.

Mas mesmo embora tenhamos recusado a oferta, ela ainda está lá, e está cada dia mais atraente, na medida que descobrimos um pouco mais das reais intenções das Big Techs com nossos dispositivos a cada dia que passa.

# Attributions and Special Thanks

## Opening track: Impact Event (by LUKHASH)

More about LUKHASH:

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: Rasengan (by Turbo Knight)

More about Turbo Knight:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about MrSuicideSheep:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


