---
title: "Nano XNO: Design do Protocolo - Parte 1"
date: 2023-07-05T12:08:31-03:00
summary: " Neste vídeo, faço uma leitura do Living White Paper da criptomoeda Nano (XNO), numa tentativa de melhor compreender seu funcionamento.  O escopo deste e dos próximos vídeos se restringe a seção \"Protocol Design\".  E neste primeiro vídeo, abordamos apenas as seguintes subseções:  - Introduction - Ledger - Blocks"
draft: false
---

{{< odysee src="https://odysee.com/$/embed/@CypherpunksToday:3/nano-white-paper-pt1:8" >}} 

Lbry: lbry://@CypherpunksToday#3/nano-white-paper-pt1#8

Odysee: https://odysee.com/@CypherpunksToday:3/nano-white-paper-pt1:8


White Paper: https://docs.nano.org/living-whitepaper/

## Description

Neste vídeo, faço uma leitura do Living White Paper da criptomoeda Nano (XNO), numa tentativa de melhor compreender seu funcionamento.

O escopo deste e dos próximos vídeos se restringe a seção "Protocol Design".

E neste primeiro vídeo, abordamos apenas as seguintes subseções:

- Introduction
- Ledger
- Blocks

# Attributions and Special Thanks

## Opening track: **Impact Event** (by **LUKHASH**)

More about **LUKHASH**: 

- Website: https://www.lukhash.com/#!licensing.html
- Spotify: https://open.spotify.com/artist/3hvgLXeDFNiqDOVXl0xTge
- YouTube: https://www.youtube.com/lukhashdotcom

## Ending track: **Rasengan** (by **Turbo Knight**)

More about **Turbo Knight**:

- Website: https://turboknight.com/
- Bandcamp: https://turboknight.bandcamp.com/
- SoundCloud: https://soundcloud.com/turboknight

## Special Thanks

Agradecimento especial ao **Cypherpunks Brasil** (https://cypherpunks.com.br) por disponibilizar a arte do logo, os artigos fenomenais e a inspiração para criar este canal.

Special thanks to **MrSuicideSheep** for the awesome Synthwave Cyberpunk Mixtapes | Volume One and Volume Two.

More about **MrSuicideSheep**:

- YouTube: https://www.youtube.com/@MrSuicideSheep
- LinkTree: https://linktr.ee/MrSuicideSheep
- Synthwave Cyberpunk Mixtape | Volume One: https://www.youtube.com/watch?v=y2ECgOhoDGs
- Synthwave Cyberpunk Mixtape | Volume Two: https://www.youtube.com/watch?v=hY02oEKfPfg


