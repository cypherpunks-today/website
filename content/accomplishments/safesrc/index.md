---
title: "Safesrc"
date: 2022-12-28T11:12:51-03:00
draft: false
---

Em Novembro de 2021, encontrei uma vulnerabilidade que explorava uma falha no controle de acesso aos dados de pagamentos feitos em BTC nos seguintes sites:

- [SafeSrc](https://safesrc.com)
- [Ancapsu](https://ancap.su)
- [Visão Libertária](https://visaolibertaria.com)

Essa vulnerabilidade permitia que o atacante visse quantos satoshis (fração de BTC) foram enviados para cada um dos colaboradores que contribuiram com a elaboração, revisão, produção e publicação de conteúdo nessas plataformas.

Isso só foi possível por uma falha no controle de acesso, que utilizava dados do Local Storage para averiguar se o usuário tinha autorização para ver tais dados.

Bastou eu alterar as informações necessárias no Local Storage, que pude me passar por administrador e tive acesso a esses dados sensíveis.

## Vídeos

Vídeo do canal SafeSrc falando sobre a vulnerabilidade:

{{< youtube nffKjL1qAn4 >}}
(https://www.youtube.com/watch?v=nffKjL1qAn4)

Veja também o vídeo que eu enviei para a equipe do Ricardo Albuquerque (dono dos sites citados). O áudio está bem ruim, pois tive que gravar com o microfone do próprio notebook. Veja o vídeo abaixo:

{{< youtube VbyBxbbeNXI >}}
(https://www.youtube.com/watch?v=VbyBxbbeNXI)
