---
title: "iOS"
date: 2022-12-28T13:15:49-03:00
draft: false
---

Em 2011, quando eu ainda era pré-adolescente, ganhei meu primeiro iPhone (iPhone 3G usado do meu pai).

Como sempre me interessei por Segurança da Informação, não demorou muito até que começasse a procurar vulnerabilidades nele.

Eu lembro do dia que vi um vídeo do [@videosdebarraquito](https://www.youtube.com/@videosdebarraquito) e fiquei impressionado com a possibilidade de burlar a tela de login, obter informações e executar ações dentro do sistema do celular.

Esse foi o vídeo que me inspirou a procurar vulnerabilidades: 
https://www.youtube.com/watch?v=MDkLpj3MM-c

Após passar uns dias tentando reproduzir a vulnerabilidade, comecei a encontrar variações da mesma. E, como o passar dos meses, fui encontrando novas vulnerabilidades e as compartilhando com o @videosdebarraquito via Twitter.

## Vulnerabilidades Encontradas

Aqui está a playlist com todas os bugs e vulnerabilidades que encontrei no iOS:
{{< yt-playlist PLZgBtraffnKEvpl2V863NIQi2JkKaYOa9 >}}
(https://www.youtube.com/playlist?list=PLZgBtraffnKEvpl2V863NIQi2JkKaYOa9)

Vale a pena notar que em um dos vídeos eu utilizo a Siri no iPhone 3G, o que não seria possível se não fosse pelos pacotes não oficiais que existiam no Cydia. A Siri foi lançada no iPhone 4, não haviam pacotes oficiais para instalar nos 3Gs.

## Mídia

Fui citado duas vezes em um blog sobre produtos da Apple:

- [Encuentran una nueva manera de saltarse el codigo de bloqueo del iPhone en iOS 6.1.3 \[video\]](http://www.movidaapple.com/blog/2013/06/11/encuentran-una-nueva-manera-de-saltarse-el-codigo-de-bloqueo-del-iphone-en-ios-6-1-3-video/)

- [Logran saltarse la clave de bloqueo del iPhone usando Siri en iOS 6.1.3 \[video\]](http://www.movidaapple.com/blog/2013/07/18/logran-saltarse-la-clave-de-bloqueo-del-iphone-usando-siri-en-ios-6-1-3-video/)

