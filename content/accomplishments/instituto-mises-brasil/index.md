---
title: "Instituto Mises Brasil"
date: 2022-12-28T12:12:18-03:00
draft: false
---

Em Agosto de 2020, encontrei uma série de 3 vulnerabilidades no portal do Instituto Mises Brasil. As vulnerabilidades eram as seguintes:

- XSS Não-Persistente
- XSS Persistente
- Falha no Controle de Acesso aos perfis de usuário

Decidi procurar por vulnerabilidades após o Instituto Mises Brasil ter sido alvo de ataques de crackers. Meu intúito era ajudar a fortalecer o movimento Libertário ao apresentá-los vulnerabilidades antes que fossem exploradas por atacantes maliciosos.

Todas elas foram éticamente encontradas e reportadas à equipe do Instituto Mises Brasil, mesmo embora tenham sido rejeitadas pela equipe.

Na época, eles, alegadamente, tinham contratado uma empresa para encontrar e corrigir as vulnerabilidades, utilizando isso como justificativa para recusar qualquer informação acerca das vulnerabilidades que eu tinha encontrado.

Confesso que fiquei um pouco desapontado com a postura deles, mas definitivamente não fiquei surpreso. O nível de segurança que encontrei provavelmente era um reflexo do quanto eles se importavam com InfoSec naquela época.

Após cerca de 2 anos do ocorrido, e após averiguar que as falhas haviam sido corrigidas, venho a publicar esse feito, de forma a incluí-lo em meu portfólio.

## Vídeo

Veja abaixo meu vídeo sobre as vulnerabilidades encontradas:

{{< youtube B1jtEjESbhU >}}
(https://www.youtube.com/watch?v=B1jtEjESbhU)

## Relatório

Ainda sem nenhuma experiência profissional na elaboração de relatórios de segurança da informação, eu havia compilado um documento com os passos necessários para reproduzir cada vulnerabilidade.

Veja o relatório no PDF a seguir: 
[Ver Relatório](docs/Vulnerabilidades.pdf)
