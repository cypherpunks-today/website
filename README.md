# Cypherpunks Today

Site do canal (Cypherpunks Today)[https://odysee.com/@CypherpunksToday:3].

Nele, falamos sobre Privacidade, Criptografia e Liberdade.

Você encontra:

- Tutoriais
- Explicações Técnicas
- Leituras Comentadas

Tudo relacionado aos movimentos Cypherpunk, Criptoanarquista e Agorista.

## Desenvolvimento

Para rodar uma cópia local, a fim de fazer modificações, basta

1. Ter o (Hugo Framework)[https://gohugo.io/] instalado
2. Ter o (git)[https://git-scm.com/] instalado
3. Rodar os comandos abaixo

```
git clone --recursive https://gitlab.com/cypherpunks-today/website.git

cd website

hugo server
```

4. Visitar http://localhost:1313/

Atribuições:

- (Hugo - Toha Theme)[https://github.com/hugo-toha/toha]
- (Hugo Framework)[https://gohugo.io/]
- (Business vector created by studiogstock - www.freepik.com)[https://www.freepik.com/vectors/business] 
