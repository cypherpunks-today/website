function showSnackBar() {
  var sb = document.getElementById("snackbar");

  //this is where the class name will be added & removed to activate the css
  sb.className = "show";

  setTimeout(()=>{ sb.className = sb.className.replace("show", ""); }, 3000);
}

function copyToClipboard(index) {
  navigator.clipboard.writeText(document.getElementById('address-' + index).innerText)
  showSnackBar();
}
